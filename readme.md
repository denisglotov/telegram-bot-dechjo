Telegram bot Dechjo
===================

This telegram bot uses python2 and is designed to be hosted on appengine.
Based on code of https://github.com/yukuku/telebot repository.


Commands
--------

    sudo apt-get install google-cloud-sdk-app-engine-python google-cloud-sdk-app-engine-python-extras
    pipenv install
    pipenv shell
    gcloud app deploy
    dev_appserver.py --port 8888 --admin_port 8889 app.yaml
    # http://localhost:8888/me

See the deployed code at https://telegram-bot-dechjo.appspot.com/me.


Reference
---------

1. App.yaml reference:
   https://cloud.google.com/appengine/docs/standard/python/config/appref

2. Downloading Your Source Code
   https://cloud.google.com/appengine/docs/standard/python/tools/downloading-source-code

3. Getting Started with Flask on App Engine Standard Environment
   https://cloud.google.com/appengine/docs/standard/python/getting-started/python-standard-env
